import {Component, HostListener} from '@angular/core';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
})
export class AppComponent {
	isEnable: boolean = false;
	title = 'ashishmondalOrgChart';
	top;
	left;
	@HostListener('window:click', ['$event'])
	handleKeyDown(event: KeyboardEvent) {
		if (!this.isEnable) {
			// this.top = event.clientY + 'px';
			// this.left = event.clientX + 'px';
			this.isEnable = !this.isEnable;
		}
	}
	topEmployee: any = {
		name: 'Horse',
		imageUrl: 'assets/images/horse3.jpg',
		// designation: 'CEO',
		subordinates: [
			{
				name: 'Horse',
				imageUrl: 'assets/images/horse3.jpg',
				// designation: 'VP',
				subordinates: [
					{
						name: 'Horse',
						imageUrl: 'assets/images/horse3.jpg',
						// designation: 'Budget Analyst',
						subordinates: [],
					},
				],
			},
			{
				name: 'Horse',
				imageUrl: 'assets/images/horse3.jpg',
				// designation: 'VP',
				subordinates: [
					{
						name: 'Horse',
						imageUrl: 'assets/images/horse3.jpg',
						// designation: 'Web Manager',
						subordinates: [],
					},
					{
						name: 'Horse',
						imageUrl: 'assets/images/horse3.jpg',
						// designation: 'Art Director',
						subordinates: [],
					},
				],
			},
			{
				name: 'Horse',
				imageUrl: 'assets/images/horse3.jpg',
				// designation: 'VP',
				subordinates: [],
			},
		],
	};
	itemClick(e, a) {
		console.log(e);
		// this.isEnable = true;
	}
}
